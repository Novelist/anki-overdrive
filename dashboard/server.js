var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var port = process.env.PORT || 3000;

var carsMsg = undefined;
var trackMsg = undefined;
var curStatus = undefined;

app.use(express.static(__dirname + '/public'));
app.use('/controller', express.static(__dirname + '/controller'));

io.on('connection', function(socket){
    console.log("New connection")

    if(curStatus != undefined) {
        socket.emit('update', curStatus);
    }

    if(trackMsg != undefined) {
        socket.emit('update', trackMsg);
        if(carsMsg != undefined) {
            socket.emit('update', carsMsg);
        }
    }

    socket.on('raceControl', function(cmd){
        console.log("Broadcasting " + cmd)
        socket.broadcast.emit('raceControl', cmd)
    });

    socket.on('update', function(msg) {
        console.log("Received update type: " + msg.type);    
        switch(msg.type){
            case "status":
                curStatus = msg;
                socket.broadcast.emit('update', msg);
                break;
            case "car":
                socket.broadcast.emit('update', msg);
                break;
            case "cars":
                carsMsg = msg;
                if(trackMsg != undefined) 
                    socket.broadcast.emit('update', msg);
                break;
            case "track":
                trackMsg = msg;
                socket.broadcast.emit('update', msg);
                if(carsMsg != undefined)
                    socket.broadcast.emit('update', carsMsg);
                break;
            default:
                break;
        }
    })

    socket.on('cmd', function(msg) {
        socket.broadcast.emit('cmd', msg);
    })
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});