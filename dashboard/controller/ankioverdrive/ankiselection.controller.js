sap.ui.controller("ankioverdrive.ankiselection", {

	onInit: function() {
		page2 = new sap.ui.view({id:"idanki2", viewName:"ankioverdrive.anki", type:sap.ui.core.mvc.ViewType.XML});
		app.addPage(page2);
	
		//attach event handler for incoming events regarding the subscription
		ws.on('update', function(msg) {
			if(msg.type == 'cars') {
				var carId = [];
				var carName = [];
				sap.ui.getCore().byId("idanki1--tileContainer").removeAllTiles();
				var cars = msg.data;
				for(var i = 0; i < cars.length; i++) {
					carId.push(cars[i].id);
					carName.push(cars[i].data.name);
					var tile = new sap.m.CustomTile().addStyleClass("sapMTile " + carName[i]).attachPress(function(oEvent) {
						var counter= oEvent.getSource().getId().charAt((oEvent.getSource().getId().length-1));
						sap.ui.getCore().byId("idanki2").getController().setTextValues(carId[counter], carName[counter]);	
						app.to(page2);
						inputCount++;
					});
					sap.ui.getCore().byId("idanki1--tileContainer").addTile(tile);
				}
			}
		});	
	}
});

var inputCount = 0;