sap.ui.controller("ankioverdrive.anki", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf ankicontroller.anki
*/
	onInit: function() {
	},
	
	//Prepare messages for websocket interaction with nodeJS application	
	//stop cars
	onStop : function(evt) {
		sendStop();
	},
	//change speed / acceleration of cars
	onChangeSpeed : function(evt) {
		source = evt.getSource().getId();
		var speed;
		//check, whether source is slider or a button to speed up/down
		if(source == "idanki2--slider") {speed = evt.getSource().getValue();}

		else {
			speed = sap.ui.getCore().byId("idanki2--slider").getValue();
			if(source == "idanki2--speedUp") {speed = speed + 10;}
			else if (source == "idanki2--speedDown") {speed = speed - 10;}
			sap.ui.getCore().byId("idanki2--slider").setValue(speed);
		}
		
		speed = speed*(400/90)+250;
		sendSpeed(speed);
	},
	//change the lanes of cars 
	onChangeLane : function(evt) {
		source = evt.getSource().getId();
		if(source == "idanki2--right") {
			if(offset <= -68) {
				offset = -67;
			} else {
				offset = offset + 9;
			}
		} 
		else {
			if(offset >= 68) {
				offset = 67;
			} else {
				offset = offset - 9;
			}
		}
		sendOffset(offset);
	},
	
	//change layout, when slider state is changed
	onChangeSwitch: function(evt) {
		var state = evt.getSource().getState();
		//enable/disable motion sensing
		if(state == true) {
			enableControls(false);
			//enable motion events
			if (window.DeviceOrientationEvent) {
				window.addEventListener('deviceorientation', handleOrientation, false);
			} else {
				sap.ui.getCore().byId("idanki2--deviceMotion").setText("Device Orientation is not supported");
			}
		} else {
			enableControls(true);
			sap.ui.getCore().byId("idanki2--slider").setValue(45);
			if (window.DeviceOrientationEvent) {
				window.removeEventListener('deviceorientation', handleOrientation);
			}
		}
	},
	
	//change view when clicking back to selection screen
	onNavBack: function(evt) {
		var dialog = new sap.m.Dialog({
			title: 'Confirm',
			type: 'Message',
			content: new sap.m.Text({ text: 'Are you sure you want to exit the game?' }),
			beginButton: new sap.m.Button({
				text: 'Cancel',
				press: function () {
					dialog.close();
				}
			}),
			endButton: new sap.m.Button({
				text: 'Submit',
				press: function () {
					sendStop();
					app.back();
					dialog.close();
				}
			}),
			afterClose: function() {
				dialog.destroy();
			}
		});
		dialog.open();
	},
	
	//change displayed text
	setTextValues: function(pCarId, pCarName) {
		carId = pCarId;
		carName = pCarName;
		sap.ui.getCore().byId("idanki2--carName").setText(pCarName);
		sap.ui.getCore().byId("idanki2--carId").setText(pCarId);
	}
});

var speed = 0;
var offset = 0;
var carId, carName;

//handle orientation events for motion sensing
function handleOrientation(evt) {
	var orientation = evt.gamma; //orientation from -90 to 90 degrees
	if(orientation < 0) {
		sap.ui.getCore().byId("idanki2--deviceMotion").setText("Please turn your device by 180 degrees to enable motion control").addStyleClass("red");
	} else {
		if(speed != Math.round((90 - orientation)/9)) {
			speed = Math.round((90 - orientation)/9);
			value = Math.round((90-orientation)*(400/90)+250);
			sap.ui.getCore().byId("idanki2--speed").setText(value);
			//sap.ui.getCore().byId("idanki1--update").setText(value);
			sendSpeed(speed);
		}
		//speed = Math.round((90 - orientation)/9);
		//sap.ui.getCore().byId("idanki1--orientation").setText(speed);	
		sap.ui.getCore().byId("idanki2--slider").setValue(Math.round(90-orientation));	
		//send message after rotation equals ten
	}	
}

//method for enabling/disabling serveral controls
function enableControls(evt) {
	sap.ui.getCore().byId("idanki2--slider").setEnabled(evt);
	sap.ui.getCore().byId("idanki2--speedUp").setEnabled(evt);
	sap.ui.getCore().byId("idanki2--speedDown").setEnabled(evt);
}

function sendStop() {
	var msg = {
		carId: carId,
		cmd: "e"
	}
	console.log("send stop")
	ws.emit('cmd', msg)
}

function sendSpeed(speed) {
	var msg = {
		carId: carId,
		cmd: "s " + speed
	}
	ws.emit('cmd', msg);
}

function sendOffset(offset) {
	var msg = {
		carId: carId,
		cmd: "c " + offset
	}
	ws.emit('cmd', msg);
}