/*
 * 
 * Created by D064914 and D064864
 * C3.js is used for the charts (http://c3js.org/).
 * If you want to use your own WS-Server, change the wsUrl below.
 * If the chart does not fit your screen, just change the height of the chartPanel in the view.
 * 
 */




// Link to the socket.io server
var wsUrl = "localhost:3000";
var websocket;
// Used to save the GaugeCharts
var charts = {};
// These set the GaugeCharts and the Tiles when no data for a car is received in one second, causes performance problems
//var timeouts = {};
var oBindings = [];
var chart;
// These are important to prevent the Charts from getting updated while they are deleted and / or (re)created
var initializingTrack = true;
var initializingCars = true;

sap.ui.controller("ankioverdrivedashboard.Dashboard", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf ankioverdrivedashboard.Dashboard
*/
	onInit: function() {
		this.initWSConnection();
	},
	
	initTrackModel: function(tracks){
		var oModel = sap.ui.getCore().getModel("trackModel");
		if(oModel === undefined){
			var oData = {
				tracks: []
			}
			oModel = new sap.ui.model.json.JSONModel(oData);
			sap.ui.getCore().setModel(oModel, "trackModel");
		}
		oModel.oData.tracks = tracks;
		oModel.refresh();
	    initializingTrack = false;
	},
	
	initCarModel: function(cars){
		var length = 1
		if(cars.length > 0) {
			length = 12 / cars.length;
		}
		sap.ui.getCore().byId("gridLayout").setDefaultSpan("L" + length + " M" + length + " S" + length);
		sap.ui.getCore().byId("dashboard").rerender();
		var oModel = sap.ui.getCore().getModel("mainModel");
		if(oModel != undefined){
			oModel.destroy();
		}
		var oData = {
				cars:[]
		}
		var oModel = new sap.ui.model.json.JSONModel(oData);
		var oBindingModel = new sap.ui.model.Binding(oModel, "/", oModel.getContext("/"));
		sap.ui.getCore().setModel(oModel, "mainModel");
		
		var carData = oModel.oData.cars;

		for(var i = 0; i < cars.length; i++){
			carObj = cars[i];
			car = {};
			car ["carId"] = carObj.id;
			car ["carName"] = carObj.data.name;
			// car ["offset"] = cars[i].offset;
			// car ["selectedBy"] = cars[i].selectedBy;
			car ["speed"] = 0;
			car ["fastestLap"] = 0;
			car ["lastLap"] = 0;
			car ["timeDifference"] = 0;
			car ["batteryStatus"] = 0;
			car ["rank"] = 0;
			car ["position"] = {trackId: 0, trackPos: 0};
			car ["lap"] = 0;
			car ["delocalized"] = false;
			// car ["status"] = 0;
			car ["lastTwoLaps"] = {currentLap: [cars[i].id + ":" + cars[i].name], lastLap: [cars[i].id + ":" + cars[i].name + "lastLap" ]};
			carData.push(car);
		};
		oModel.refresh();
		sap.ui.getCore().byId("gridLayout").rerender();

		// Set data binding here
		for(var n = 0; n < oBindings.length; n++){
			oBindings[n].detachChange();
		}
		var c = this;
		for(var j = 0; j < cars.length; j++){
			var oBinding = oModel.bindProperty("/cars/" + j + "/speed");
			oBinding.attachChange(function(){
				var path = this.sPath;
				var carIndex = path.split("/")[2];
				if(!initializingTrack && !initializingCars){
					c.updateGaugeCharts(carIndex);
				}
				
				if(typeof chart != "undefined" && !initializingTrack && !initializingCars){
					c.updateChart(carIndex);
				}
				
				if(!initializingTrack && !initializingCars && typeof chart != "undefined"){
					c.updateBatteryStatus(carIndex);
				}
			});
			oBindings.push(oBinding);
		};
		
		this.makeChart();
		var c = this;
		setTimeout(function(){
			c.makeGaugeCharts();
		}, 2000);
		
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf ankioverdrivedashboard.Dashboard
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf ankioverdrivedashboard.Dashboard
*/
//	onAfterRendering: function() {
//		
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf ankioverdrivedashboard.Dashboard
*/
//	onExit: function() {
//
//	}

	
	
/*
 * This part is used to connect to the broker via 
 * a websocket connection and process the messages.
 */
	initWSConnection: function() {
		var controller = this;
		websocket = io(wsUrl);
		websocket.on('connect', function(){
			console.log("Connected.")
		});

		websocket.on('update', function(msg){
			controller.processMessage(msg);
		});	
		websocket.on('disconnect', () => {
			console.log("Disconnected. Trying to reconnect...")
  			websocket.open();
		});
	},
	
	closeWSConnection: function() {
		websocket.close();
	},
	
	onMenuItemSelected: function(string){
		websocket.emit('raceControl', {cmd: string.toLowerCase()});
	},

	processMessage: function(msg) {	
		console.log(msg);
		var type = msg.type;
		switch(type){
			case "status":
				sap.ui.getCore().byId("label_status").setText(msg.data.status);
				break;
			case "track":
				initializingTrack = true;
				this.initTrackModel(msg.data);
				break;
			case "cars":
				initializingCars = true;
				this.initCarModel(msg.data);
				break;
			case "car":
				var oModel = sap.ui.getCore().getModel("mainModel");
				var oData = oModel.getData().cars;
				var data = msg.data;
				console.log(data);
				var carId = data.id;
				for(var i = 0; i < oData.length; i++){
					if(oData[i].carId == carId){
						oData[i].carId = carId;
						oData[i].carName = data.name;
						oData[i].speed = data.speed;
						// oData[i].offset = json.offset;
						// oData[i].selectedBy = json.user;
						oData[i].fastestLap = data.race.fastestLap;
						oData[i].lastLap = data.race.lastLap;
						oData[i].timeDifference = data.race.timeDifference;
						oData[i].batteryStatus = data.batteryLevel;
						oData[i].rank = data.race.rank;
						// oData[i].position = {trackId: json.trackId, trackPos: json.trackPos};
						oData[i].lap = data.race.lap;
						oData[i].delocalized = data.position.delocalized;
						// oData[i].status = json.status;
						oData[i].lastTwoLaps.currentLap.push(data.speed);
						if(data.position.trackId == 33) {
							oData[i].lastTwoLaps.lastLap.length = 0;
							oData[i].lastTwoLaps.currentLap.shift();
							oData[i].lastTwoLaps.lastLap[0] = carId + ":" + data.name + "lastLap";
							oData[i].lastTwoLaps.lastLap.push.apply(oData[i].lastTwoLaps.lastLap, oData[i].lastTwoLaps.currentLap);
							oData[i].lastTwoLaps.lastLap[0] = carId + ":" + data.name + "lastLap";
							oData[i].lastTwoLaps.currentLap = [""+carId + ":" + data.name]; 
						}
					}
				}
				oModel.refresh();
				break;
			default:
				break;
		}
		// // var deviceName = data.deviceName;
		// // var deviceType = data.deviceType;
		// // var param = data.param;
		// // var value = data.value;
		// if(deviceType == "AnkiController"){
		// 	switch(param){
		// 	case "tracks":
		// 		initializingTrack = true;
		// 		var json = JSON.parse(data.value);
		// 		var tracks = json.tracks;
		// 		this.initTrackModel(tracks);
		// 		break;
		// 	case "cars":
		// 		initializingCars = true;
		// 		var json = JSON.parse(data.value);
		// 		var cars = json.cars;
		// 		this.initCarModel(cars);
		// 		break;
		// 	case "status":
		// 		break;
		// 	}
		// } else {
		
	},
	
	
/*
 * These functions are used to update css 
 */
	
	
	updateBatteryStatus: function(carIndex){
		var oCarData = this.getView().getModel("mainModel").oData;
		var carId = oCarData.cars[carIndex].carId;
		var carName = oCarData.cars[carIndex].carName;
		var newBatteryLevel = oCarData.cars[carIndex].batteryStatus;
		var doc = document.getElementById("" + carId + carName);
		var currentWidth = doc.style.width;
		currentWidth = currentWidth.substring(0, currentWidth.length - 2);
		// max battery: 4250 = 194px
		var currentBatteryLevel = currentWidth * 4250 / 194 
		if (newBatteryLevel > 4250) {
			newBatteryLevel = 4250;
		}
		if (newBatteryLevel <= currentBatteryLevel) {
			currentBatteryLevel = newBatteryLevel;
			var value = currentBatteryLevel / 4250 * 194;
			doc.style.width = value + "px";
		}
	},
	
	changeStatusTile: function(carId, carName, status){
		var doc = document.getElementById("" + carName + carId);
		if(doc != null)
		{
			var cN = doc.className;
			if(!cN.includes(status + "Tile")){
				switch(status){
				case "ok":
					doc.className = doc.className.replace("warningTile", status + "Tile");
					break;
				case "warning":
					doc.className = doc.className.replace("okTile", status + "Tile");
					break;
				}
			}
		}
	},
	
	
/*
 * These functions are used to create and update the Gauge Charts
 */
	makeGaugeCharts: function(){
		for(key in charts){
			charts[key] = charts[key].destroy();
		}
		charts = {};
		sap.ui.getCore().byId("gridLayout").rerender();
		var oModel = sap.ui.getCore().getModel("mainModel");
		var oData = oModel.oData;
		var cars = oModel.oData.cars;
		var c = this;
		for(var i = 0; i < cars.length; i++){
			var divID = '#' + cars[i].carId;
			// makes a gauge chart that belongs to a car. Contains no data yet
			charts[cars[i].carId]=(	
				c3.generate({
					bindto : divID,
					data : {
						columns : [["",0]] ,
						type : 'gauge',
					},
					gauge : {
						label : {
							format : function(value, ratio) {
								return value;
							},
							// to turn off the min/max labels.
							show : true
						},
						min : 0, 
						max : 1200, 
						units : ' mm/s',
						width : 39
					},
					color : {
						// The tree color levels for the percentage values
						pattern : [ '#60B044', '#F6C600', '#F97600', '#FF0000' ],
						threshold : {
							unit : 'value', 
							max : 200, 
							values : [ 300, 600, 900, 1000 ]
						}
					},
					size : {
						height : 150
					}
				})
			);
		};
		sap.ui.getCore().byId("batteryHTML").rerender();
		sap.ui.getCore().byId("carTileHTML").rerender();
		initializingCars = false;
	},
	
	updateGaugeCharts: function(carIndex) {
		var size = 0;
		for(key in charts){
			if(charts.hasOwnProperty(key)) size++;
		}
		if(size > 0 && !initializingTrack && !initializingCars && typeof chart != "undefined"){
			var oCarData = this.getView().getModel("mainModel").oData;
			var carId = oCarData.cars[carIndex].carId;
			var speed = oCarData.cars[carIndex].speed;
			charts[carId].load({
				columns: [["",speed]] 
			});
			
		};
		
	},
	
	clearGauge: function(carId){
		var size = 0;
		for(key in charts){
			if(charts.hasOwnProperty(key)) size++;
		}
		if(size > 0 && !initializingTrack && !initializingCars && typeof chart != "undefined"){
			charts[carId].load({
				columns: [["",0]] 
			});
		}
	},
	
	
/*
 * These functions are used to create and update the Chart
 */
	makeChart: function(){
		var oModel = sap.ui.getCore().getModel("mainModel");
		var oData = oModel.oData;
		cars = oModel.oData.cars;
		colors = {};
		for(var i = 0; i < cars.length; i++){
			
			var color;
			var string = cars[i].lastTwoLaps.currentLap[0];
			var split = string.split(":");
			switch(split[1]){
			case "groundshock":
				color = 'rgba(29,140,199,1)';
				break;
			case "skull":
				color = 'rgba(242,30,32,1)';
				break;
			case "bigbang":
				color = 'rgba(83,101,77,1)';
				break;
			case "guardian":
				color = 'rgba(40,37,37,1)';
				break;
			case "nuke":
				color = 'rgba(173,201,64,1)';
				break;
			case "thermo":
				color = 'rgba(177,0,12,1)';
				break;
			}	
			
			var colorLastLap;
			string = cars[i].lastTwoLaps.lastLap[0];
			var split = string.split(":");
			switch(split[1]){
			case "groundshocklastLap":
				colorLastLap = 'rgba(29,140,199,0.5)';
				break;
			case "skulllastLap":
				colorLastLap = 'rgba(242,30,32,0.5)';
				break;
			case "bigbanglastLap":
				colorLastLap = 'rgba(83,101,77,0.5)';
				break;
			case "guardianlastLap":
				colorLastLap = 'rgba(40,37,37,0.5)';
				break;
			case "nukelastLap":
				colorLastLap = 'rgba(173,201,64,0.5)';
				break;
			case "thermolastLap":
				colorLastLap = 'rgba(177,0,12,0.5)';
				break;
			}
			
			
			colors[cars[i].lastTwoLaps.currentLap[0] + ""] = color; 
			colors[cars[i].lastTwoLaps.lastLap[0] + ""] = colorLastLap; 

		}
		
		var panelHeight = sap.ui.getCore().byId("chartPanel").getHeight();
		panelHeight = panelHeight.substr(0,panelHeight.length-2);
		panelHeight = panelHeight - 35;
		var oTrackModel = sap.ui.getCore().getModel("trackModel");
		var tracks = oTrackModel.oData.tracks;
		var divID = '#chart';
	    chart = c3.generate({
	    	
	    	bindto: divID,

	        data: {
	        	columns: [
	        	 ],
	            colors: colors
	        },
		    axis: {
		        x: {
		           tick: {
		            	values: function(){
		            		var vals = [];
		            		for (var i = 0; i<tracks.length;i++	) {
		            			vals.push(i);
		            		}
		            		return vals;
		            	},
		            },
		            padding: {
		            	right: 1
		            },
		            max: tracks.length
		            
		        },
	        	y: {
	        		tick: {
	        			values: [0,50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000, 1050, 1000,1150]
	        		},
	        		padding: {
	        			top: 1
	        		}
//	        		max: 700,
//	        		min: 0,
	        	}
		    },
		    size: {
		    	height: panelHeight
		    }
	    });  
	},
	
	updateChart: function(carIndex){
		oModel = sap.ui.getCore().getModel("mainModel");
			chart.load({
				columns: [
		                      oModel.oData.cars[carIndex].lastTwoLaps.currentLap,
		                      oModel.oData.cars[carIndex].lastTwoLaps.lastLap,
		                ],
			});
			
			if(oModel.oData.cars[carIndex].position.trackId == '33'){
	

				chart.load({ 
					data: {
						columns: [
			                      oModel.oData.cars[carIndex].lastTwoLaps.lastLap,
								],
					
					},
				});
			}
			
		
	}
	
	
});