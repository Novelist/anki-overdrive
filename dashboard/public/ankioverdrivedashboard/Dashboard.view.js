sap.ui.jsview("ankioverdrivedashboard.Dashboard", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ankioverdrivedashboard.Dashboard
	*/ 
	getControllerName : function() {
		return "ankioverdrivedashboard.Dashboard";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ankioverdrivedashboard.Dashboard
	*/ 
	createContent : function(oController) {
		
		var oMenu = new sap.m.Menu({
			items: [
			        new sap.m.MenuItem({
			        	text: "Start Scan",
			        	icon: "sap-icon://car-rental",  
					}),
					new sap.m.MenuItem({
			        	text: "Init new Race",
			        	icon: "sap-icon://refresh",  
			        }),
			        new sap.m.MenuItem({
			        	text: "Start Race",
			        	icon: "sap-icon://begin",  
			        }),
			        new sap.m.MenuItem({
			        	text: "Stop Race",
			        	icon: "sap-icon://stop",  
			        }),
			        ],
			itemSelected: function(oEvent){
				oController.onMenuItemSelected(oEvent.getParameter("item").getText());
			},		
		});
		
		var oToolbar = new sap.m.Toolbar({
	    	  content: [
	    	            new sap.ui.core.HTML({
	    	            	content:'<div class="logo"></div>',
	    	            	preferDOM: true
	    	            }),
	    	            new sap.ui.core.HTML({
	    	            	content: '<div class="headerText">Race Track Vehicle Insights</div>',
	    	            	preferDOM: true
						}),
						new sap.m.Label("label_status", {
							text: "No Update",
						}).addStyleClass("statusLabel"),
	    	            new sap.m.Button({
	    	            	text: "Refresh",
	    	            	press: function(){
	    	            		location.reload(true);
	    	            	}
	    	            }).addStyleClass("refreshButton"),
	    	            new sap.m.MenuButton({
	    	            	icon: "sap-icon://arrow-down",
	    	            	menu: oMenu
	    	            }).addStyleClass("navArrow"),
	    	  ]
	      }).addStyleClass("header");
		
		
		
		
		var oBattery =    new sap.ui.core.HTML("batteryHTML",{
		  	  preferDOM: false,
	    	  content: '<div class="carColorCell margin"><div style="width:194px;" id="{mainModel>carId}{mainModel>carName}" class="carColorCellBefore cellBackground{mainModel>carName}"></div><div class="carColorCellAfter"></div></div>'
	      });
		var oCarTile = new sap.ui.core.HTML("carTileHTML",{
		  	  preferDOM: false,
    		  content: '<div id="{mainModel>carName}{mainModel>carId}" class="carTile okTile tile{mainModel>carName} margin"></div>'
    	  });
		
		var oGaugeChart = new sap.ui.core.HTML({
  		  	  preferDOM: false,
	    	  content: '<div id="{mainModel>carId}" class="content margin"></div>'
	      });
		
		var oRankLabel = new sap.m.Label({
	    	  design: sap.m.LabelDesign.Bold,
	    	  text: "#{mainModel>rank}"
	      }).addStyleClass("marginTop rankLabelFourCars");
		
		var oTimeDiffLabel = new sap.m.Label({
	    	  text: {
	    		  path: "mainModel>timeDifference",
	    		  formatter: function(oVal){
					  if(oVal == undefined || oVal == null) return "--";
	    			  if(oVal > 100 || oVal < -100){
	    				  oVal = 0;
	    				  return "" + oVal + "s";
	    			  } else {
	    				  if(oVal < 0){
	    					 if(this.hasStyleClass("timeDifferencePositive")){
	    						 this.removeStyleClass("timeDifferencePositive");
	    					 }
    						 this.addStyleClass("timeDifferenceNegative");
	    				  } else if(oVal > 0){
	    					  if(this.hasStyleClass("timeDifferenceNegative")){
	    						 this.removeStyleClass("timeDifferenceNegative");
	    					 } 
	    					  this.addStyleClass("timeDifferencePositive");
	    					  oVal = "+" + oVal;
			    			  oVal = oVal.substr(0, 6) + "s";
			    			  return oVal;
	    				  } else {
	    					  if(this.hasStyleClass("timeDifferenceNegative")){
		    						 this.removeStyleClass("timeDifferenceNegative");}
	    					  if(this.hasStyleClass("timeDifferencePositive")){
		    						 this.removeStyleClass("timeDifferencePositive");}
	    				  }
	    				  oVal = oVal + "";
		    			  oVal = oVal.substr(0, 6) + "s";
		    			  return oVal;
	    			  }
	    			  
	    		  }
	    	  }
	      }).addStyleClass("timeDifference").addStyleClass("timeDifferencePositive");
		
		var oFastestLapHeaderLabel = new sap.m.Label({
	    	  text: "Fastest Lap:"
	      }).addStyleClass("marginTop infoHeaderFourCars");
		
		var oFastestLapContentLabel =  new sap.m.Label({
	    	text: {
	    		  path: "mainModel>fastestLap",
	    		  formatter: function(oVal){
					  if(oVal == undefined || oVal == null) return "--";
	    			  if(oVal > 1000){
	    				  oVal = 0;
	    				  return "" + oVal + "s";
	    			  }
	    			  oVal = oVal + "";
	    			  oVal = oVal.substring(0,4) + "s";
	    			  return oVal;
	    		  }
	    		  }
	      }).addStyleClass("infoTextFourCars");
		
		var oLastLapHeaderLabel = new sap.m.Label({
	    	  text:"Last Lap:"
	      }).addStyleClass("marginTop infoHeaderFourCars");
		
		var oLastLapContentLabel =  new sap.m.Label({
	    	  text:{
	    		  path: "mainModel>lastLap",
	    		  formatter: function(oVal){
					  if(oVal == undefined || oVal == null) return "--";
	    			  if(oVal > 1000){
	    				  oVal = 0;
	    				  return "" + oVal + "s";
	    			  }
	    			  oVal = oVal + "";
	    			  oVal = oVal.substring(0,4) + "s";
	    			  return oVal;
	    		  }
	    	  }
	      }).addStyleClass("infoTextFourCars");
		
		var oLapHeaderLabel =  new sap.m.Label({
	    	  text:"Laps"
	      }).addStyleClass("marginTop infoHeaderFourCars");
		
		var oLapContentLabel =  new sap.m.Label({
	    	  text:"{mainModel>lap}"
	      }).addStyleClass("infoTextFourCars");
		
		
		
		var oCarBox = new sap.m.Panel ({
			content: [
			          new sap.m.FlexBox({
			        	  fitContainer: true,
			        	  displayInline: true,
			        	  justifyContent: sap.m.FlexJustifyContent.SpaceAround,
			        	  alignItems: sap.m.FlexAlignItems.Stretch,
			        	  alignContent: sap.m.FlexAlignContent.Center,
			        	  items: [
			        	          new sap.m.VBox({}).addItem(oBattery).addItem(oCarTile).addItem(oGaugeChart),
						          new sap.m.VBox({}).addStyleClass("marginLeft")
						          .addItem(oRankLabel)
						          .addItem(oTimeDiffLabel)
						          .addItem(oFastestLapHeaderLabel)
						          .addItem(oFastestLapContentLabel)
						          .addItem(oLastLapHeaderLabel)
						          .addItem(oLastLapContentLabel)
						          .addItem(oLapHeaderLabel)
						          .addItem(oLapContentLabel), 
			        	  ]
			          })
			]
		});
			
		var oGridLayout =  new sap.ui.layout.Grid("gridLayout", {
     	   position: sap.ui.layout.GridPosition.Center,
    	   defaultSpan: "L3 M3 S3",
    	   content:{
    		   path: "mainModel>/cars",
    		   template: oCarBox  
    	   } 
       });
		
		var chartPanel = new sap.m.Panel("chartPanel", {
	    	 width: "100%",
	    	 height: "545px",
	    	 layoutData: new sap.ui.layout.GridData({
				span:"L12 M12 S12",
				linebreakL:true,
				linebreakM:true,
				linebreakS:true,
			}),
			content: new sap.ui.core.HTML({
				content: '<div id="chart"></div>'
			})
	     });    
		
		var oGridLayoutChart = new sap.ui.layout.Grid({
			position: sap.ui.layout.GridPosition.Center,
			defaultSpan: "L12 M12 S12",
				content: [chartPanel]
		});
		
		
		
 		return new sap.m.Page("dashboard", {
 			showHeader: false,
 			showSubHeader: false,
 			enableScrolling: false,
 			showFooter: false,
			content: [
			      new sap.ui.core.HTML({
			    	  content:'<div class="blueline"></div>'
			      }),
			      oToolbar,
			      oGridLayout,
			      oGridLayoutChart
			]
		});
	}

});