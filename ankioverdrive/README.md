## Node.js Controller

The controller connects to the cars and is able to simulate a race. It connects as a socket.io client.

### Setup for Raspberry Pi
This works with a Raspberry Pi 2 Model B. **There is an image for the Pi 2 Model B, too. So you just have to install the image and it will run!**
Otherwise you should be able to set it up with the following documentation, too. Setting it up on other Pi versions should work to, but is not tested yet.

While installing Noobs, make sure that you unplug LAN. Plug it in when you set the proxy.

1. Configure .profile file

  + Navigate to /home/pi/.profile (you may have to rightclick and then “show hidden files” to see the .profile file)
  
  + `export http_proxy=“http://proxy.wdf.sap.corp:8080/”`
  + `export https_proxy=”http://proxy.wdf.sap.corp:8080/”`
  
2. Create 10proxy file

  + `cd /etc/apt/apt.conf.d`
  + `sudo nano 10proxy`
  + `Acquire::http::Proxy “http://proxy.wdf.sap.corp:8080/”;`
  
3. Reboot

4. Update the OS
  
  + `sudo apt-get update`
  + `sudo apt-get upgrade`

5. Install Node v4 and copy to /usr/local
  
  + `wget https://nodejs.org/dist/v4.4.7/node-v4.4.7-linux-armv7l.tar.gz`
  + `tar -xvf node-v4.4.7-linux-armv7l.tar.gz`
  + `cd node-v4.4.7-linux-armv7l`
  + `sudo cp -R * /usr/local/`
   
  + To check Node.js is properly install and you have the right version, run the command node –v
  
6. Set proxy and python for npm
 
  + `npm config set http-proxy http://proxy.wdf.sap.corp:8080/`
  + `npm config set https-proxy http://proxy.wdf.sap.corp:8080/`
  + `npm config set python python2.7`
  
7. Get the controller files, make sure that bluetooth-hci-socket is not listed within the dependencys in package.json because it already comes with noble

8. Install linux packages for bluetooth

  + `sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev`

9. Install node modules

  + `npm install`

10. Run controllerSAP.js with sudo

  + `sudo node controllerSAP.js`



### Setup for Windows

Make sure you have the following tools installed and bound to your path.


+ <a href="https://nodejs.org/en/download/">Node.JS</a>
+ <a href="https://docs.npmjs.com/getting-started/installing-node">NPM</a>
+ <a href="https://git-scm.com/downloads">Git</a>


After installing all the required application, run following commands in the command line

`git clone https://github.wdf.sap.corp/d049416/HCP_Anki_Overdrive` <br />
`cd HCP_Anki_Overdrive`<br />
`npm install`<br />

Install <a href="https://www.python.org/download/releases/2.7/">Python 2.7</a> and <a href="https://www.visualstudio.com/en-us/products/visual-studio-express-vs.aspx">Visual Studio</a>.
To enable the BLE connection to the cars the package you need one of the follwing Bluetooth adapters:

<table>
  <tr>
    <th>Name</th>
    <th>USB VID</th>
    <th>USB PID</th>
  </tr>
  
  <tr>
    <td>BCM920702 Bluetooth 4.0</td>
    <td>0x0a5c</td>
    <td>0x21e8</td>
  </tr>
  
  <tr>
    <td>BCM20702A0 Bluetooth 4.0</td>
    <td>0x19ff</td>
    <td>0x0239</td>
  </tr>
  
  <tr>
    <td>CSR8510 A10</td>
    <td>0x0a12</td>
    <td>0x0001</td>
  </tr>
  
  <tr>
    <td>Asus BT-400</td>
    <td>0x0b05</td>
    <td>0x17cb</td>
  </tr>
</table>

After you have the correct adapter, a WinUSB driver is required wherefore the <a href="http://zadig.akeo.ie/">Zadig</a> tool can be used to replace the driver for your adapter.

Run the following commands to get this programm running

`npm install bluetooth-hci-socket`<br />
`npm install noble`<br />
`npm install https-proxy-agent`<br />



### Setup for OS X

1. Install nodejs from http://nodejs.org
2. Get the repository from https://github.wdf.sap.corp/d049416/HCP_Anki_Overdrive
3. Configure npm proxy
  + `npm config set proxy http://proxy.wdf.sap.corp:8080/`
  + `npm config set http-proxy http://proxy.wdf.sap.corp:8080/`
  + `npm config set https-proxy http://proxy.wdf.sap.corp:8080/`
4. Add python to npm
  + `npm config set python2.7`
5. Install node modules
  + `npm install`
6. The bluetooth-hci-socket module may fail to install
  + If that happens clone the repository from https://github.com/sandeepmistry/node-bluetooth-hci-socket into your node_modules folder
  + Alternatively, download as a zip and unzip it in your node_modules folder
  + Make sure it is in a folder named bluetooth-hci-socket
