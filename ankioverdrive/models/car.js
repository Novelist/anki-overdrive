var async = require('async'),
    os = require('os');

// Car object. Handles everything that is related to a car.

var Car = function(id, name, detected, peripheral, serviceUuids, raceHandler, cars, socket){
    this.id = id;
    this.detected = detected;
    this.peripheral = peripheral;
    this.serviceUuids = serviceUuids;
    this.raceHandler = raceHandler;
    this.cars = cars;
    this.socket = socket;
    this.characteristic = undefined;
    this.readCharacteristic = undefined;
    this.version = undefined;
    this.pingResponse = undefined;
    this.initialOffset = undefined;
    this.isActive = false;
    this.scanningCar = false;
    this.isReady = false;
    this.isInitialized = false;
    // This is the node we will send out plus the car id
    this.data = {
        "name": name,
        "batteryLevel": undefined,
        "speed": undefined,
        "position": {
            // "offset": undefined,
            // "offsetFromRoad": undefined, // May delete that
            "trackId": undefined,
            "trackPos": undefined,
            "delocalized": false,
            "trackIndex": 0,
            "buffer": {
                "trackLastPos": undefined,
                "trackLastId": undefined
            }
        },
        "race": {
            "fastestLap": undefined,
            "lastLap": undefined,
            "rank": undefined,
            "lap": undefined,
            "timeDifference": undefined,
        }
    }
}

Car.prototype.setCharacteristic = function(characteristic){
    this.characteristic = characteristic
}

Car.prototype.setReadCharacteristic = function(readCharacteristic){
    this.readCharacteristic = readCharacteristic
}

Car.prototype.setVersion = function(version){
    this.version = version
}

Car.prototype.setPingResponse = function(pingResponse){
    this.pingResponse = pingResponse
}

// Set car up by identifying the characteristics which are used to read from and write to the car
Car.prototype.setUp = function() {
    var peripheral = this.peripheral;
    var that = this;

    peripheral.once('connect', function (err) {
        if(err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Connecting to ' + peripheral.uuid);
        peripheral.discoverServices([], function (err, services) {
            var service;
            if (os.platform() === 'win32' || os.platform() === 'linux') {
                service = services[2];
            } else {
                service = service[0]; // macOS
            }
            service.discoverCharacteristics([], function (err, characteristics) {
                    var characteristicIndex = 0;

                    async.whilst(
                        function() {
                            return (characteristicIndex < characteristics.length);
                        },
                        function(callback) {
                            var characteristic = characteristics[characteristicIndex];
                            async.series([
                                function(callback) {
                                    if (characteristic.uuid == 'be15bee06186407e83810bd89c4d8df4') {
                                        // Read characteristic
                                        that.readCharacteristic = characteristic;
                                        characteristic.notify(true, function (err) {
                                            // This is mandatory to trigger receiving the data, altough it is not used for the actual reading (Yes, this makes zero sense).
                                        });
                                        characteristic.on('read', function (data, isNotification) {
                                            //console.log("Data: " + data);
                                            that.handleMessage(data)
                                        });
                                    }

                                    if (characteristic.uuid == 'be15bee16186407e83810bd89c4d8df4') {
                                        // Write characteristic
                                        that.setCharacteristic(characteristic);
                                        that.init();
                                    }

                                    callback();
                                },
                                function(){
                                    characteristicIndex++;
                                    callback();
                                }
                            ]);
                        },
                        function(error) {
                            if(error != null) console.log("Error: " + error)
                        }
                    )
                });
        });
    });

    peripheral.connect();

    // peripheral.connect(function (err) {
    //     if(err) {
    //         console.log(err);
    //         process.exit(1);
    //     }
    //     console.log('Connecting to ' + peripheral.uuid);
    //     peripheral.discoverServices([], function (err, services) {
    //         console.log(services);
    //         var service;
    //         if (os.platform() === 'win32' || os.platform() === 'linux') {
    //             service = services[2];
    //         } else {
    //             service = service[0]; // macOS
    //         }
    //         service.discoverCharacteristics([], function (err, characteristics) {
    //                 var characteristicIndex = 0;

    //                 async.whilst(
    //                     function() {
    //                         return (characteristicIndex < characteristics.length);
    //                     },
    //                     function(callback) {
    //                         var characteristic = characteristics[characteristicIndex];
    //                         async.series([
    //                             function(callback) {
    //                                 if (characteristic.uuid == 'be15bee06186407e83810bd89c4d8df4') {
    //                                     // Read characteristic
    //                                     that.readCharacteristic = characteristic;
    //                                     characteristic.notify(true, function (err) {
    //                                         // This is mandatory to trigger receiving the data, altough it is not used for the actual reading (Yes, this makes zero sense).
    //                                     });
    //                                     characteristic.on('read', function (data, isNotification) {
    //                                         //console.log("Data: " + data);
    //                                         that.handleMessage(data)
    //                                     });
    //                                 }

    //                                 if (characteristic.uuid == 'be15bee16186407e83810bd89c4d8df4') {
    //                                     // Write characteristic
    //                                     that.setCharacteristic(characteristic);
    //                                     that.init();
    //                                 }

    //                                 callback();
    //                             },
    //                             function(){
    //                                 characteristicIndex++;
    //                                 callback();
    //                             }
    //                         ]);
    //                     },
    //                     function(error) {
    //                         if(error != null) console.log("Error: " + error)
    //                     }
    //                 )
    //             });
    //     });
    // });
}

// Write init message to car. Is mandatory for the car to work well.
Car.prototype.init = function() {
    var initMessage = new Buffer(4);
    initMessage.writeUInt8(0x03, 0);
    initMessage.writeUInt8(0x90, 1);
    initMessage.writeUInt8(0x01, 2);
    initMessage.writeUInt8(0x01, 3);
    var that = this;
    console.log("Initializing " + this.id);
    this.characteristic.write(initMessage, false, function (err) {
        if (!err) {
            that.isInitialized = true;
            console.log(that.id + " is initialized")
            if(that.cars.checkIfAllCarsAreInitialized()) that.socket.sendStatusUpdate("CARS_INITIALIZED")
        } else {
            console.log('Error while initializing ' + this.name);
        }
    });
}

// Invokes any command that is valid
Car.prototype.invokeCommand = function(command) {
    var message = this.formatMessage(command);
        if (message) {
            if (this.characteristic) {
                this.characteristic.write(message, false, function (err) {
                    if (err) {
                        console.log('Error sending command1');
                    }
                });
            } else {
                console.log('Error sending command2');
            }
        } else {
            console.log('No message supplied');
    }
}

// Validates and formats message for the car based on the command given
Car.prototype.formatMessage = function(command){
    var message = undefined,
        cmd = undefined,
        commandArray = undefined;
    if (command.indexOf(' ') == -1) {
        cmd = command; // only one command send
    } else {
        commandArray = command.split(' '); // multiple commands send
        if (commandArray.length > 0) {
            cmd = commandArray[0];
        }
    }
    switch (cmd) {
        case 's': // set speed, accelerate
            var speed = 500,
                accel = 12500;
            if (commandArray.length > 1) {
                speed = commandArray[1]; // speed is send
            }
            if (commandArray.length > 2) {
                accel = commandArray[2]; // acceleration is send
            }
            message = new Buffer(7);
            message.writeUInt8(0x06, 0);
            message.writeUInt8(0x24, 1);
            message.writeInt16LE(speed, 2);
            message.writeInt16LE(accel, 4);
            break;
        case 'e': // end, set speed to zero
            message = new Buffer(7);
            message.writeUInt8(0x06, 0);
            message.writeUInt8(0x24, 1);
            message.writeInt16LE(0x00, 2);
            message.writeInt16LE(12500, 4);
            break;
        case 'c': // change lane
            var offset = 0.0
            if (commandArray && commandArray.length > 1) {
                offset = commandArray[1];
            }
            message = new Buffer(12);
            message.writeUInt8(11, 0);
            message.writeUInt8(0x25, 1);
            message.writeInt16LE(250, 2);
            message.writeInt16LE(1000, 4);
            message.writeFloatLE(offset, 6);
            break;
        case 'o': // set offset
            var offset = -68.0;
            if (commandArray && commandArray.length > 1) {
                offset = commandArray[1];
            }
            message = new Buffer(6);
            message.writeUInt8(0x05, 0);
            message.writeUInt8(0x2c, 1);
            message.writeFloatLE(-68.0, 2);
            break;
        case 'l': // change lights
            message = new Buffer(3);
            message.writeUInt8(0x02, 0);
            message.writeUInt8(0x1d, 1);
            message.writeUInt8(140, 2);
            break;
        case 'bat': // request battery level
            message = new Buffer(2);
            message.writeUInt8(0x01, 0);
            message.writeUInt8(0x1a, 1);
            break;
        case 'q': // quit and disconnect
            message = new Buffer(2);
            message.writeUInt8(0x01, 0);
            message.writeUInt8(0x0d, 1);
            break;
        default:
            console.log('The command ' + cmd + ' isn\'t set yet');
    }
    return message;
}

// Detects the current lane the car is on by finding the nearest point. 
// Used for initializing the car as it needs its initial offset (the lane you put the car on) as a reference.
Car.prototype.detectCurrentLane = function(){
    var trackId = this.data.position.trackId;
    var trackPos = this.data.position.trackPos;
    var bufferedTrackId = this.data.position.buffer.trackLastId;
    var bufferedTrackPos = this.data.position.buffer.trackLastPos;
    var that = this;
  
    if(bufferedTrackPos != undefined && bufferedTrackId != undefined){
        var direction = trackPos - bufferedTrackPos;

        // determine the current lane
        if(trackPos == 0 || trackPos == 1 || trackPos == 2) {
            // console.log('car: ' + car.name + ' on lane 1 with: ' + trackId + ' pos: ' + trackPos);
            return -68.0 * direction;
        }else if(trackPos == 15 || trackPos == 16 || trackPos == 17) {
            // console.log('car: ' + car.name + ' on lane 2 with: ' + trackId + ' pos: ' + trackPos);
            return -23.0 * direction;
        }else if(trackPos == 30 || trackPos == 31 || trackPos == 32) {
            // console.log('car: ' + car.name + ' on lane 3 with: ' + trackId + ' pos: ' + trackPos);
            return 23.0 * direction;
        } else if(trackPos == 45 || trackPos == 46 || trackPos == 47) {
            // console.log('car: ' + car.name + ' on lane 1 with: ' + trackId + ' pos: ' + trackPos);
            return 68.0 * direction;
        } else {
            // console.log('car: ' + car.name + ' between the lanes with: ' + trackId + ' pos: ' + trackPos);
            // determine the offset between lane 4 and 3
            if(trackPos == 3 || trackPos == 4 || trackPos == 5) {
                return -59.0 * direction;
            }
            else if(trackPos == 6 || trackPos == 7 || trackPos == 8) {
                return -50.0 * direction;
            }
            else if(trackPos == 9 || trackPos == 10 || trackPos == 11) {
                return -41.0 * direction;
            }
            else if(trackPos == 12 || trackPos == 13 || trackPos == 14) {
                return -32.0 * direction;
            }

            // determine the offset between lane 3 and 2
            else if(trackPos == 18 || trackPos == 19 || trackPos == 20) {
                return -14.0 * direction;
            }
            else if(trackPos == 21 || trackPos == 22 || trackPos == 23) {
                return -5.0 * direction;
            }
            else if(trackPos == 24 || trackPos == 25 || trackPos == 26) {
                return 5.0 * direction;
            }
            else if(trackPos == 27 || trackPos == 28 || trackPos == 29) {
                return 14.0 * direction;
            }

            // determine the offset between lane 2 and 1
            else if(trackPos == 33 || trackPos == 34 || trackPos == 35) {
                return 32.0 * direction;
            }
            else if(trackPos == 36 || trackPos == 37 || trackPos == 38) {
                return 41.0 * direction;
            }
            else if(trackPos == 39 || trackPos == 40 || trackPos == 41) {
                return 50.0 * direction;
            }
            else if(trackPos == 42 || trackPos == 43 || trackPos == 44) {
                return 59.0 * direction;
            }
        }
    }
    return undefined;
}

// Init the offset. Mandatory for the car to work well.
Car.prototype.initOffset = function(offset){
    var initMessage = new Buffer(6);
    initMessage.writeUInt8(0x05, 0);
    initMessage.writeUInt8(0x2c, 1);
    initMessage.writeFloatLE(offset, 2);
    var that = this;
    this.characteristic.write(initMessage, false, function (err) {
        if (!err) {
            that.initialOffset = offset;
            console.log("Car " + that.id + " is at offset " + offset);
            return that;
        } else {
            return undefined;
        }
    });
}

// Handles received messages
Car.prototype.handleMessage = function(data) {
    // Check carId to determine which car is currently sending data
    var carId = this.id
    var messageId = data.readUInt8(1);
    var date = new Date();
    var that = this;

    if (messageId == '23') {
        // example: <Buffer 01 17>
        var desc = 'Ping Response Received';
        // console.log('Message: ' + messageId, data, desc);
        that.setPingResponse(data);
    }

    else if (messageId == '25') {
        // example: <Buffer 05 19 6e 26 00 00>
        var desc = 'Version Received';
        var version = data.readUInt16LE(2);
        // console.log('Message: ' + messageId, data, desc + ' - version: ' + version);
        that.setVersion(version);
    }

    else if (messageId == '27') {
        // example: <Buffer 03 1b 50 0f>
        var desc = 'Battery Level Received';
        var level = data.readUInt16LE(2);
        //console.log('Message: ' + messageId, data, desc + ' - level: ' + level);
        that.data.batteryLevel = level;
    }

    else if (messageId == '39') {
        // example: <Buffer 10 27 21 28 48 e1 86 c2 02 01 47 00 00 00 02 fa 00>
        //var desc = 'Localization Position Update Received';
        var pieceLocation = data.readUInt8(2);
        var pieceId = data.readUInt8(3); // in my starter kit:
        // 1 x straight: 36
        // 1 x straight: 39
        // 1 x straight: 40
        // 1 x curve: 20
        // 1 x curve: 23
        // 2 x curve: 18
        // 2 x curve: 17
        // 1 x start/finish: 34 (long) and 33 (short)
        var offset = data.readFloatLE(4);
        that.data.position.offset = offset;
        var speed = data.readUInt16LE(8);
        that.data.speed = speed;
        var pieceLocation = data.readUInt8(2);
        that.data.position.buffer.trackLastPos = that.data.position.trackPos;
        that.data.position.trackPos = pieceLocation;
		var pieceId = data.readUInt8(3);
        that.data.position.buffer.trackLastId = that.data.position.trackId;
        that.data.position.trackId = pieceId;
        if(pieceId == 33) {
            that.data.position.trackIndex = 0;
        } else {
            that.data.position.trackIndex++;
        }
        
        that.raceHandler.positionListener(that);

        //console.log('Message: ' + messageId, data, desc + ' - offset: '  + offset + ' speed: ' + speed + ' - pieceId: '  + pieceId + ' pieceLocation: ' + pieceLocation);
    }

    else if (messageId == '41') {
        // example: <Buffer 12 29 00 00 02 2b 55 c2 00 ff 81 46 00 00 00 00 00 25 32>
        var desc = 'Localization Transition Update Received';
        var offset = data.readFloatLE(4);
        that.data.position.offset = offset;
        // console.log('Message: ' + messageId + ', ' + 'Localization Transition Update Received, Offset: ' + offset);
        // if(scanning == true) {
        //     console.log('Message: ' + messageId, data, desc + ' - offset: ' + offset);
        // }
    }

    else if (messageId == '43') {
        // example: <Buffer 01 2b>
        var desc = 'Vehicle Delocalized Received';
        // console.log('Message: ' + messageId, data, desc);
        that.data.position.delocalized = true;
    }

    else if (messageId == '45') {
        // example: <Buffer 06 2d 00 c8 75 3d 03>
        var desc = 'Offset from Road Center Update Received';
        var offset = data.readFloatLE(2);
        // console.log('Message: ' + messageId, data, desc + ' - offset: '  + offset);
        that.data.position.offsetFromRoad = offset;
        // if(wSocket) {
        //     messageHandler.sendWSMessage(wSocket, 'UPDATE_F', 'AnkiDemo', carId, Car.prototype.name, 'AnkiCar', 'offsetfromroad', offset, 'na');
        // }
    }

    else if (messageId == '54') {
        // example: <Buffer 03 36 00 00>
        // console.log('Message: ' + messageId, data, 'Not documented');
    }

    else if (messageId == '63') {
        // example: <Buffer 05 3f 01 00 00 01>
        // 0 for away 1 for on the loading station
        var away = data.readUInt8(3);
        if(away == 0){
            that.isActive = true;
            that.raceHandler.carListener(that);
        } else if(away == 1){
            that.isActive = false;
            that.raceHandler.carListener(that);
        }
        // console.log('Message: ' + messageId, data.readUInt8(3), data, 'Not documented');
    }

    else if (messageId == '65') {
        // example: <Buffer 0e 41 9a 99 7f 42 9a 99 7f 42 00 00 00 02 81>
        var desc = 'Changed Offset (not documented)';
        var offset = data.readFloatLE(2);
        // console.log('Message: ' + messageId, data, desc + ' - offset: '  + offset);
    }

    else if (messageId == '67') {
        // example: <Buffer 01 43>
        // console.log('Message: ' + messageId, data, 'Not documented');
    }

    else if (messageId == '77') {
        // example: <Buffer 03 4d 00 01>
        // console.log('Message: ' + messageId, data, 'Not documented');
    }

    else if (messageId == '134') {
        // example: <Buffer 0b 86 8e 00 27 08 00 00 10 10 00 00>
        // console.log('Message: ' + messageId, data, 'Not documented');
    }

    else if (messageId == '201') {
        // example: tbd
        // console.log('Message: ' + messageId, data, 'Not documented');
    }

    else {
        // console.log('Message: ' + messageId, data, 'Unknown');
    }

		
}

module.exports = Car