var events = require('events');

var trackArray = [];
var raceStartedAt = undefined;

// Status vars
var scanning = false;
var choosedScanningCar = false;
var scanningDone = false;
var carsAreGettingPositioned = false;

function donePositioning(){
    carsAreGettingPositioned = false;
}

module.exports = {
    racing: false,
    cars: undefined,
    socket: undefined,
    scanningDoneListener: function(array, car){
        scanningDone = true;
        scanning = false;
        this.trackLength = trackArray.length;
        this.socket.sendTrack(trackArray);
        lastVal = undefined;
        trackCopy = [];
        for(var i = 0; i < trackArray.length; i++){
            var val = trackArray[i];
            if (trackArray[i] != lastVal) trackCopy.push(val);
            lastVal = val;
        }
        trackArray = trackCopy;
        // This is important that the cars have enough time to switch the line before receiving another overwriting command. It get's reset in donePositioning()
        carsAreGettingPositioned = true;
        this.cars.invokeCommand('s 300');  
        this.cars.positionCarsOnLane(donePositioning);
    },
    positionListener: function(car){
        car.invokeCommand('bat');
        var trackId = car.data.position.trackId;
        var trackPos = car.data.position.trackPos;

        // If a car is not intialized and it is currently on a straight piece
        if(car.initialOffset == undefined && (trackId == 36 || trackId == 39 || trackId == 40)) {
            var offset = car.detectCurrentLane();
            if (offset != undefined) {
                car.initOffset(offset);
            }
        }


        if (this.racing == false && scanningDone == true && trackId == 34 && (trackPos % 2 == 0)) {
            // Scan done, but not all cars are on start lane
        } else if (this.racing == false && scanningDone == true && trackId == 34 && (trackPos % 2 != 0) && !car.scanningCar && !carsAreGettingPositioned) {
            // Stop car at startLane
            car.invokeCommand('e');
            car.isReady = true;

            // Check if all cars are ready
            var allCarsReady = true;
            for (var i = 0, len = this.cars.connectedCars.length; i < len; i++) {
                var car = this.cars.connectedCars[i];
                if (!car.isReady) {
                    allCarsReady = false;
                    break;
                }
            }
            if (allCarsReady) {
                var a = this.cars.connectedCars;
                for (var i = 0; i < a.length; i++) {
                    a[i].isReady = false;
                }
                this.socket.sendStatusUpdate("READY");
            }
        }
        
        if(!scanningDone && !choosedScanningCar && car.data.position.trackId == 33){
            // The first car that reaches the start lane becomes the scanning car
            choosedScanningCar = true;
            scanning = true;
            car.scanningCar = true;
            trackArray.push(33);
        } else if(!scanningDone && choosedScanningCar && car.scanningCar) {
            // If the car is the scanning car add trackId to trackArray or, if it is done scanning, trigger the scanningDoneListener()
            if(car.data.position.trackId == 33) {
                car.scanningCar = false;
                this.scanningDoneListener();
            }
            trackArray.push(car.data.position.trackId);
        }

        if (this.racing) { 
            // Update stats and send update for the car
            var raceData = car.data.race;
            var currentTime = new Date().getTime();
            if(trackId == 33) {
                if(raceData.lastLapBeganAt != undefined) {
                    var curLapTime = (currentTime - raceData.lastLapBeganAt)/1000;
                    raceData.timeDifference = curLapTime - raceData.lastLap;
                    raceData.lastLap = curLapTime;
                    if(raceData.fastestLap == undefined || isNaN(raceData.fastestLap) || raceData.fastestLap > raceData.lastLap)
                        raceData.fastestLap = raceData.lastLap;
                    raceData.lap++;
                }
                raceData.lastLapBeganAt = currentTime;
            }
            this.cars.checkCurrentRanks();
            this.socket.sendCarUpdate(car);
        }
    },
    carListener: function(car) {
        // Gets triggerred when a car enters or leaves the loading station.
        if(!car.isActive) {
            var index = this.cars.connectedCars.indexOf(car);
            this.cars.connectedCars.splice(index, 1);
        } else {
            if(this.cars.connectedCars.indexOf(car) < 0)
                this.cars.connectedCars.push(car);
        }
        this.socket.sendCars(this.cars.connectedCars);
    },
    startNewScan: function() {
        this.socket.sendStatusUpdate("TRACKSCAN");
        if(!this.racing && !scanningDone && !scanning) {
            // Start scan
            this.cars.invokeCommand("s 200");
        } else if(scanningDone && !scanning) {
            // New scan
            trackArray = [];
            scanningDone = false;
            this.racing = false;
            raceStartedAt = undefined
            choosedScanningCar = false;
            this.cars.invokeCommand("s 200");
        } else if(scanning) {
            // Do nothing and wait
        }
    },
    initNewRace: function() {
        if(scanningDone && !scanning) {
            this.cars.invokeCommand("s 300");
            this.socket.sendStatusUpdate("INITRACE");
            this.cars.positionCarsOnLane(donePositioning);
            raceStartedAt = undefined;
            this.racing = false;
        }
    },
    startRace: function() {
        if(!this.racing && scanningDone && !scanning) {
            var a = this.cars.connectedCars;
            // Reset Cars
            for(var i = 0; i < a.length; i++) {
                var r = a[i].data.race;
                r.currentLapTime = undefined;
                r.fastestLap = undefined;
                r.lastLap = undefined;
                r.rank = undefined;
                r.timeDifference = undefined;
                r.lap = 0;
            }
            raceStartedAt = new Date().getTime();
            this.racing = true;
            this.socket.sendStatusUpdate("RACE");
            this.cars.invokeCommand("s 400");
        }
    },
    stopRace: function() {
        raceStartedAt = undefined;
        this.cars.invokeCommand("e");
        this.socket.sendStatusUpdate("STOP");
        this.racing = false;
    }
}