"use strict"

var Car = require("../models/car"),
    noble = require('noble');

module.exports = {
    cars: undefined,
    raceHandler: undefined,
    socket: undefined,
    addListeners: function(){
        var that = this;
        console.log('Adding BLTListeners');
        try {
            // Scan for cars
            noble.on('stateChange', function (state) {
                console.log("Noble: State change to " + state)
                if (state === 'poweredOn') {
                    console.log("Start scanning")
                    that.socket.sendStatusUpdate("SCANNING_FOR_CARS")                                                                 
                    var startScan = function() {
                        noble.startScanning();
                        // Restart scan if no car is found
                        setTimeout(function(){
                                noble.stopScanning();
                            if(that.cars.availableCars.length > 0) {
                                console.log("Stop scanning")
                                // Start car initialization
                                that.cars.initCars(); 
                            } else {
                                console.log("Scanning...")
                                startScan();
                            }
                        }, 4000);
                    } 
                    startScan();
                } else {
                    console.log('Scanning stopped');
                    noble.stopScanning();
                }
            });

            // Discovered new car
            noble.on('discover', function (peripheral) {
                    var serviceUuids = JSON.stringify(peripheral.advertisement.serviceUuids);
                    // Check if it is an Anki car or truck
                    if (serviceUuids.indexOf("be15beef6186407e83810bd89c4d8df4") > -1) {
                        var carname = that.cars.getCarName(peripheral.advertisement.manufacturerData);
                        var car = new Car(peripheral.uuid, carname, true, peripheral, serviceUuids, that.raceHandler, that.cars, that.socket);
                        console.log("Detected " + carname + "; ID: " + peripheral.uuid);
                        that.cars.availableCars.push(car);
                    }                
            });
        } catch (e) {
            console.log(e.message);
        }
    }
}