// This module handles everything that is related to ALL cars.

module.exports = {
    availableCars: [],
    connectedCars: [],
    socket: undefined,
    // Init cars by setting the flag to false (during initialization) and trigger the set up of the car
    initCars: function () { 
        console.log("Initializing cars")
        this.socket.sendStatusUpdate("INIT_CARS");
        for (var i = 0; i < this.availableCars.length; i++) {
            var currentCar = this.availableCars[i];
            currentCar.isActive = false;
            // in serviceUuids
            if (currentCar.serviceUuids.indexOf('be15beef6186407e83810bd89c4d8df4') > -1) { 
                currentCar.setUp();
            }
        }
    },
    // Get's car name by hex char at position 7 in the manufacturer data string.
    getCarName: function(manufacturerData){
        var substr = parseInt(manufacturerData.toString('hex').charAt(7), 16);
        var carname;
        switch (substr) {
            case 0:
                carname = 'x52';
                break;
            case 8:
                carname = 'groundshock';
                break;
            case 9:
                carname = 'skull';
                break;
            case 10:
                carname = 'thermo';
                break;
            case 11:
                carname = 'nuke';
                break;
            case 12:
                carname = 'guardian';
                break;
            case 13:
                //
                break;
            case 14:
                carname = 'bigbang';
                break;
        }
        return carname;
    },
    // Position cars on their lane so that they fit to the start line nicely
    positionCarsOnLane: function(callback){
        // Try to make use of the lanes on the offsets: -68 -23, 23, 68
        var lanes = [-68, -23, 23, 68];
        var availableLane = undefined;

        for (var i = 0, j = 0, len = this.connectedCars.length; i < len; i++) {
            var car = this.connectedCars[i];
            // find a free lane
            availableLane = lanes[j++];
            console.log("Car " + car.id + " sent to lane " + availableLane);
            car.invokeCommand('c ' + availableLane);
        }
        if(callback != undefined)
            var that = this;
            setTimeout(function(){
                that.invokeCommand('s 200');
            }, 6000)
            setTimeout(callback, 6000);
    },
    // Current ranks calculated by trackId and trackPos
    checkCurrentRanks: function() {
        var sortedCars = this.connectedCars.slice().sort(sortFunction)
        function sortFunction(a, b) {
            var aLap = a.data.race.lap;
            var bLap = b.data.race.lap;
            var aTrackIndex = a.data.position.trackIndex;
            var bTrackIndex = b.data.position.trackIndex;
            if (aLap === bLap && aTrackIndex === bTrackIndex) {
                return 0;
            } else {
                return ((aLap < bLap || (aLap === bLap && aTrackIndex < bTrackIndex)) ? 1 : -1);
            }
        }
    
        var rank = 0;
        for (var i = 0; i < sortedCars.length; i++) {
            var car = sortedCars[i];
            // assign the rank based on the order
            car.data.race.rank = ++rank;
            // console.log('ranking: ' + car.data.name + ' rank: ' + car.data.race.rank);
        }
    },
    // Invoke the same command to every car, mainly for testing and to stop or start the race
    invokeCommand: function(cmd){
        for(var i = 0; i < this.availableCars.length; i++){
            this.availableCars[i].invokeCommand(cmd);
        }
    },
    checkIfAllCarsAreInitialized: function(){
        var a = this.availableCars;
        var initialized = true;
        for(var i = 0; i < a.length; i++){
            if(!a[i].isInitialized) {
                initialized = false;
                break;
            }
        }
        return initialized;
    }
};