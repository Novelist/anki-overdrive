"use strict"

// Handles all the socket communication and calls the right functions in case of incoming messages.

module.exports = {
    socket: undefined,
    cars: undefined,
    raceHandler: undefined,
    url: "http://localhost:3000",
    // For faster access to the car object
    carObj: {},
    updateCars: function() {
        var array = this.cars.connectedCars;
        for(var i = 0; i < array.length; i++) {
            var car = array[i];
            this.carObj[car.id] = car;
        }
    },
    create: function () {
        var that = this;
        // proxy configuration
        /*var proxy = process.env.http_proxy || 'http://proxy.wdf.sap.corp:8080',
        opts = url.parse(proxy),
        agent = new HttpsProxyAgent(opts);*/
        this.socket = io.connect(this.url, {reconnect: true});

        this.socket.on('connect', function () {
            console.log('Connected');
        });
        this.socket.on('raceControl', function (msg) {
            switch(msg.cmd){
                case "start scan":
                    that.raceHandler.startNewScan();
                    break;
                case "init new race":
                    that.raceHandler.initNewRace();
                    break;
                case "start race":
                    that.raceHandler.startRace();
                    break;
                case "stop race":
                    that.raceHandler.stopRace();
                    break;
            }
        });
        this.socket.on('cmd', function(msg) {
            if(msg.isAdmin || that.raceHandler.racing) {
                if(msg.hasOwnProperty('carId')){
                    that.carObj[msg.carId].invokeCommand(msg.cmd);
                } else {
                    that.cars.invokeCommand(msg.cmd);
                }
            }
        });
    },
    sendStatusUpdate: function(newStatus) {
        var msg = {
            "type": "status",
            "data": {
                status: newStatus,
            }
        };
        this.socket.emit('update', msg);
    },
    sendCars: function(cars) {
        this.updateCars();
        var carsData = [];
        for(var i = 0; i < cars.length; i++){
            if(cars[i].isInitialized){
                var d = {}
                d.data = JSON.parse(JSON.stringify(cars[i].data));
                delete d.data.position.buffer;
                d.id = cars[i].id;
                carsData.push(d);
            }
        }
        var msg = {
           "type": "cars",
           "data": carsData
        }
        this.socket.emit('update', msg);
    },
    sendTrack: function(track){
        var msg = {
            "type": "track",
            "data": track
        }
        this.socket.emit('update', msg);
    },
    sendCarUpdate: function(car){
        var carData = JSON.parse(JSON.stringify(car.data));
        delete carData.position.buffer;
        carData.id = car.id;
        var msg = {
            "type": "car",
            "data": carData
        }
        this.socket.emit('update', msg);
    }    
}