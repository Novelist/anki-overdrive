/**
 * Created by D064229 on 06.07.2017.
 */

// imports
var cars = require('./lib/cars'), // car related functions
    raceHandler = require('./lib/raceHandler')
    socket = require('./lib/socket'),
    blt = require ('./lib/blt'),
    io = require('socket.io-client'); // web sockets

// Make references available where there are needed.
raceHandler.cars = cars;
raceHandler.socket = socket;
socket.cars = cars;
socket.raceHandler = raceHandler;
socket.create();
cars.socket = socket;
blt.cars = cars;
blt.raceHandler = raceHandler;
blt.socket = socket;
blt.addListeners();

// Don't exit app instantly
process.stdin.resume(); 

function exitHandler(options, err) {
    socket.sendStatusUpdate("QUIT")
    socket.socket.close();
    var len = cars.availableCars.length;
    if(len > 0) {
        for (var i; i < len; i++) {
            var currentCar = cars.availableCars[i];
            // Let the car disconnect itself
            currentCar.invokeCommand('q');
            // Disconnect us from the car
            currentCar.peripheral.disconnect(); // disconnect cars
        }
    }
    process.exit();
}

process.on('exit', exitHandler.bind(null, {cleanup: true}));
process.on('SIGINT', exitHandler.bind(null, {exit: true}));
process.on('uncaughtException', exitHandler.bind(null, {exit: true}));