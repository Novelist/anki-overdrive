# Anki Overdrive Demo

This repository enables you to use an [Anki Overdrive](https://anki.com/de-de/overdrive) set as a demo.

It comes with a Node server that connects to the car and simulates a race. You can communicate with it using socket.io.

The dashboard shows the current status live. A controller app enables you or your customers to control the cars using their phones.

The command line tool is a simple website that you can use for testing purposes.

Please check out the directories for further information.

## Setup on Raspberry Pi 3

Download the OpenUI5 SDK and copy the resources folder into dashboard/public/resources
and dashboard/controller/resources.

Then copy the whole thing onto your Raspberry Pi 3.

Place every car you want to use on the loading station. Then run dashboard/server.js and after some seconds start ankioverdrive/main.js. Now open Chromium and go to localhost:3000 to see the dashboard. Place the cars you want to use on the track and trigger start scan in the upper right corner of the dashboard. As soon as the cars stopped on the starting lane press start race.

To control the cars, go to localhost:3000/controller.

## Hot to use the demo Pi

Place the cars on the loading station and plug the Pi in. Everything is set up already, so just wait. You can connect to the Pi via WiFi with your phone. The password is raspberry. Then, go to 172.24.1.1:3000/controller to control the cars with your phone.